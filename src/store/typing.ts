export type TNewData = {
  data: any;
  displayName: string;
  id: string;
  type: string;
}
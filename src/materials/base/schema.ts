import ImageRange from './ImageRange/schema'
import Text from './Text/schema'

export const BasicSchema = {
	ImageRange,
	Text,
}
